package com.fransoft.ecobot;

import com.fransoft.telegramchatbot.AbstractTelegramLongPollingBot;
import com.fransoft.telegramchatbot.bo.CommandData;
import com.fransoft.telegramchatbot.bo.TelegramCommand;
import com.fransoft.telegramchatbot.utils.CommandLineUtils;
import java.util.HashMap;
import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class EcoBot extends AbstractTelegramLongPollingBot {

  private EcoBotProcessor ecoBotProcessor;

  public EcoBot(
      @Value("${ecobot.name}") String botUsername,
      @Value("${ecobot.apikey}") String apiKey,
      @Value("${ecobot.function}") String botFunction,
      EcoBotProcessor ecoBotProcessor) {
    this.botUsername = botUsername;
    this.apiKey = apiKey;
    this.botFunction = botFunction;

    this.ecoBotProcessor = ecoBotProcessor;

    initCommands();
  }

  private void initCommands() {
    CommandData commandDataThrowADice = CommandData.CommandDataBuilder.commandData()
        .withVerb("/msg")
        .withOptions(null)
        .withPresentation("mensaje")
        .build();

    commands = new HashMap<>();
    commands.put("msg", commandDataThrowADice);
  }

  @Override
  public void onUpdateReceived(Update update) {
    TelegramCommand tc = new TelegramCommand(update);
    if (tc.getVerb()==null){
      return;
    }

    switch(tc.getVerb()){
      case "msg":
        processMsg(update);
        break;
      case COMMAND_HELP:
        printHelp(tc);
        break;
      default:
        noImplement(tc);
    }

  }

  private void processMsg(Update update) {
    CommandData commandData = commands.get("msg");
    try {
      this.sendMessage((this.ecoBotProcessor.msg(new TelegramCommand(update, commandData.getOptions()))));
    } catch (ParseException e) {
      printMessage(new TelegramCommand(update), CommandLineUtils
          .getHelpCommandLine(commandData.getVerb(), commandData.getOptions(), commandData.getPresentation()));
    }
  }
}
