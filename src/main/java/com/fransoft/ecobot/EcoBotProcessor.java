package com.fransoft.ecobot;

import com.fransoft.telegramchatbot.bo.TelegramCommand;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Component
public class EcoBotProcessor {

  public SendMessage msg(TelegramCommand telegramCommand) {
    return telegramCommand.sendMessage(telegramCommand.getVerb(), false);
  }
}
